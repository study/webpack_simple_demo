var path = require('path');

module.exports = {
    style: {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
    },
    fonts: {
        test: /\.(eot|woff|woff2|ttf|svg)$/,
        loader: 'url-loader',
        query: {
            limit: 30000,
            name: '[name]-[ext]',
            outputPath: 'assets/fonts/'
        }
    },
    images: {
        test: /\.png$/, loader: 'file-loader',
        query: {
            name: '[name].[ext]',
            outputPath: 'assets/images/'
        }
    }
};
