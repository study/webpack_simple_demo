const webpack = require('webpack')
const path = require('path')
const rules = require('./webpack.rules')

const config = {
    entry: './index.js',
    output: {
        filename: 'bundle.js',
        path: __dirname + '/build'
    },
    resolve: {
        modules: [path.resolve(__dirname, "semantic"), "node_modules"]
    },
    module: {
        rules: [
            rules.fonts,
            rules.images,
            rules.style
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
    ],
    devServer: {
        compress: true,
        port: 9001,
        hot: true,
        historyApiFallback: {
            index: 'index.html'
        }
    }
};

module.exports = config;
